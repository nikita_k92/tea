$(document).ready(function () {

  const mobileMenuButton = $(".mobile-menu-button");
  const mobileMenuArea = $(".mobile-menu-area");
  const mobileMenu = $(".mobile-menu");
  const body = $("body");
  const subMenu = $(".sub-menu");
  const mobileMenuLink = $(".mobile-menu__link");
  const menuButton = $("#menu-button")
  const buttonFilter = $("#button-filter")
  const buttonFilterClose = $("#button-filter-close")
  const mobileFilterArea = $(".mobile-filter-area")


  buttonFilter.click(function () {
    mobileFilterArea.toggleClass("mobile-filter-area--active");
    $('.header-search-line').removeClass('header-search-line--open')
    // body.toggleClass("stop");
  })

  mobileMenuButton.click(function () {
    $(this).toggleClass("mobile-menu-button--active");
    mobileMenuArea.toggleClass("mobile-menu-area--active");
    mobileMenu.toggleClass("mobile-menu--open");
    // body.toggleClass("stop");
    subMenu.removeClass("mobile-menu--open");
    mobileFilterArea.removeClass("mobile-filter-area--active");
    $('.header-search-line').removeClass('header-search-line--open')
  });

  const renderMenuMobile = () => {
    let offsetTopMenu = $(".section-header").height();
    mobileMenuArea.css({"height": `calc(100vh - ${offsetTopMenu}px)`, "top": `${offsetTopMenu}px`});
    mobileFilterArea.css({"height": `calc(100vh - ${offsetTopMenu}px)`, "top": `${offsetTopMenu}px`});
  };

  $(window).resize(function () {
    if ($(window).width() >= 1200) {
      // $("body").removeClass("stop");
      mobileMenuArea.removeClass("mobile-menu-area--active");
      mobileMenu.removeClass("mobile-menu--open");
      mobileMenuButton.removeClass("mobile-menu-button--active");
      menuButton.prop("checked", false);
    }
    if ($(window).width() >= 992) {
      // $("body").removeClass("stop");
      mobileFilterArea.removeClass("mobile-filter-area--active");
    }
    renderMenuMobile();
  });
  renderMenuMobile();

  const openSubMenu = () => {
    mobileMenuLink.click(function () {
      const attrSubMenuId = $(this).attr("data-link-id");
      $(`.sub-menu[data-catalog-id=${attrSubMenuId}]`).addClass("mobile-menu--open");
    });
  };
  const closeSubMenu = () => {
    subMenu.click(function () {
      $(this).removeClass("mobile-menu--open");
    });
  };

  const closeFilterMenu = () => {
    buttonFilterClose.click(function () {
      mobileFilterArea.removeClass("mobile-filter-area--active");
      // $("body").removeClass("stop");
    })
  }

  openSubMenu();
  closeSubMenu();
  closeFilterMenu();

});
