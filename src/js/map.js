$(document).ready(function() {
  ymaps.ready(init);
  let destinations = {
    'OR': [55.851606, 37.665165],
  };
  function init() {
    // Создание экземпляра карты и его привязка к контейнеру с
    // заданным id ("map").
    myMap = new ymaps.Map('map', {
      // При инициализации карты обязательно нужно указать
      // её центр и коэффициент масштабирования.
      center: destinations['OR'],
      zoom: 17
    });
    let myPlacemark = new ymaps.Placemark(destinations['OR'], {
      // Хинт показывается при наведении мышкой на иконку метки.
      hintContent: "Москва, ул. Уржумская, д. 4",
      // Балун откроется при клике по метке.

    }, {
      //опции
      iconLayout: 'default#image',
      iconImageHref: 'img/map-point.png',
      iconImageSize: [52, 61],
      iconImageOffset: [-27, -54],
    });
    myMap.geoObjects.add(myPlacemark);
  }
})
