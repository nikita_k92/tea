$(".range-price").slider({
  animate: "slow",
  range: true,
  values: [ 10, 1000 ],
  slide : function(event, ui) {
    $( ".result-range-price > .from > input" ).val(ui.values[ 0 ]);
    $( ".result-range-price > .to > input" ).val( ui.values[ 1 ])
  }
});
$( ".result-range-price > .from > input" ).val($(".range-price").slider("values", 0) );
$( ".result-range-price > .to  > input" ).val($(".range-price").slider("values", 1) );
