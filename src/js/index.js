import './menu'
import './map'
import './range'

$(document).ready(function () {


  let x = [".icon-svg"];
  x.forEach(item => {
    $(item).each(function () {
      let $img = $(this);
      let imgClass = $img.attr("class");
      let imgURL = $img.attr("src");
      $.get(imgURL, function (data) {
        let $svg = $(data).find("svg");
        if (typeof imgClass !== "undefined") {
          $svg = $svg.attr("class", imgClass + " replaced-svg");
        }
        $svg = $svg.removeAttr("xmlns:a");
        if (!$svg.attr("viewBox") && $svg.attr("height") && $svg.attr("width")) {
          $svg.attr("viewBox", "0 0 " + $svg.attr("height") + " " + $svg.attr("width"));
        }
        $img.replaceWith($svg);
      }, "");
    });
  });

  new Swiper('.swiper-sert', {
    loop: false,
    scrollbar: {
      el: ".swiper-scrollbar",
      // hide: true,
      draggable: true,
    },
    // slidesPerView: 3,
    // spaceBetween: 30,
    // slidesPerGroup: 3,
    breakpoints: {
      // when window width is <= 499px
      576: {
        slidesPerView: 1,
        slidesPerGroup: 1,
        spaceBetweenSlides: 30
      },
      // when window width is <= 999px
      767: {
        slidesPerView: 2,
        slidesPerGroup: 1,
        spaceBetweenSlides: 30
      },
      1000: {
        slidesPerView: 4,
        slidesPerGroup: 1,
        spaceBetweenSlides: 30
      }
    },
  });

  new Swiper('.swiper-banner', {
    loop: true,
    effect: "fade",
    autoplay: {
      delay: 5000,
      disableOnInteraction: false,
    },
  })

  new Swiper('.swiper-recommend', {
    loop: false,
    // effect: "fade",
    slidesPerView: 1,
    slidesPerGroup: 1,
    spaceBetween: 30,
    pagination: {
      clickable: true,
      el: '.swiper-pagination',
    },
    breakpoints: {
      // when window width is <= 499px
      576: {
        slidesPerView: 1,
        slidesPerGroup: 1,
      },
      // when window width is <= 999px
      767: {
        slidesPerView: 2,
        slidesPerGroup: 1,
      },
      1000: {
        slidesPerView: 3,
        slidesPerGroup: 1,
      }
    },
  })


  $('.select-filter').select2({
    placeholder: '',
    theme: 'select-filter__theme',
    dropdownCssClass: 'select-filter__drop',
    selectionCssClass: 'select-filter__selection',
    allowClear: false,
    closeOnSelect: true,
  });

  $('.select-type-product').select2({
    placeholder: '',
    dropdownAutoWidth: true,
    theme: 'select-type-product__theme',
    dropdownCssClass: 'select-type-product__drop ',
    selectionCssClass: 'select-type-product__selection ',
    allowClear: false,
    closeOnSelect: false,
  });
  $('.select-delivery').select2({
    placeholder: '',
    theme: 'select-filter__theme',
    dropdownCssClass: 'select-filter__drop select-delivery__drop',
    selectionCssClass: 'select-filter__selection select-delivery__selection',
    allowClear: false,
    closeOnSelect: true,
    positionDropdown: true
  });
  $('#search').click(function (e) {
    e.preventDefault()
    e.stopPropagation()
    $('.header-search-line').addClass('header-search-line--open')
  })

  $('.select-filter-image').select2({
    placeholder: '',
    theme: 'select-filter__theme',
    dropdownCssClass: 'select-filter__drop',
    selectionCssClass: 'select-filter__selection',
    allowClear: false,
    closeOnSelect: true,
    templateResult: formatState,
    templateSelection: formatStateSelection
  });

  function formatStateSelection (state) {
    if (!state.id) {
      return state.text;
    }

    var baseUrl = "img";
    var $state = $(
      '<span><img class="" /> <span></span></span>'
    );

    // Use .text() instead of HTML string concatenation to avoid script injection issues
    $state.find("span").text(state.text);
    $state.find("img").attr("src", baseUrl + "/" + state.element.value.toLowerCase() + "");

    return $state;
  };

  function formatState (state) {
    if (!state.id) { return state.text; }
    console.log(state.element)
    var $state = $(
      '<span><img src="img/' +  state.element.value.toLowerCase() +
      '" class="img-flag" /> ' +
      state.text +     '</span>'
    );
    return $state;
  };

  let show = $('.show').toArray()

  $(window).click(function(e) {
    if ($(e.target).closest(".show").length === 0) {
      show.forEach(x=>{
        $(x).removeClass("header-search-line--open")
      })
    }
  });
})
